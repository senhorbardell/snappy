# Socket.io interactions prototype. Work in progress.

# Things to do:

- split to tasks, milestones
- add estimates
- finish invite, start flow
- add game loop
- pick physics engine
- procedural pipes
- add session persistnance
- socketio elasticity, load balansing, load testing
- deploy, backend infrastructure, ci

# Questions

- If all physics will be rendering on server, need to do something about
  different ios andoid screen sizes, consult front-end

# Installation

- ```npm install```
- add PORT, zsh environment variable (1337)
- ```npm start```

# Client emit

## host

Will create session, add yourself to this session, emit sessions to all connected users

## accept :session

User accepted an invitation, client receiving new sessions payload, where the status of current user will change to accepted

## ready :session

after client loads screen and resources

## tap :session

User taps on screen in order to lift character

# Client listen

## sessions

```json
[
    {
        "session": "hash",
        "users": [
            {
                "id": 1,
                "name": "Username",
                "status": "waiting"
            }, {
                "id": 2,
                "name": "Username2",
                "status": "accepted"
            }
        ],
        "status": "waiting"
    }
]
```

## tick

Once all clients ready. Server continuously sends tick

```json
{
    "player1": {
        "id": 1,
        "name": "Player1",
        "location": {x, y}
    },
    "player2": {
        "id": 2,
        "name": "Player2",
        "location": {x, y}
    },
    "objects": [
        {"coords": [{x, y}, {x, y}]},
        {"coords": [{x, y}, {x, y}]}
    ]
}
```

Objects – array of pipes

## crash

Someone collided with pipes or floor, or timed out. Users will receive:

```json
{
    "player1": {
        "id": 1,
        "name": "Player1",
        "score": 1234
    },
    "player2": {
        "id": 2,
        "name": "Player2",
        "score": 10
    }
}
```
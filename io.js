module.exports = function(socket) {
  console.log('client connected ' + socket.id);

  socket.on('host', function() {
    var sessions = [{
      session: socket.id,
      users: [{id: socket.id, status: 'waiting'}]
    }];
    socket.broadcast.emit('sessions', sessions); // to lazy to mess with global io now, will dublicate
    socket.emit('sessions', sessions);
  });

  socket.on('accept', function(user) {
    socket.join(user);
    var sessions = [{
      session: user,
      users: [user, socket.id]
    }];
    socket.broadcast.to(user).emit('sessions', sessions);
    socket.emit('sessions', sessions);
  });

  socket.on('ready', function(session)  {
    var sessions = [{
      session: session,
      users: [{id: session, status: 'waiting'}, {id: socket.id, status: 'ready'}]
    }];
    socket.broadcast.to(session).emit('sessions', sessions);
    socket.emit('sessions', sessions);

    // check if all ready
    // then emit tick
    // if someone crashed, emit crash, destroy session
  });

};
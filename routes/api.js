var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.json({status: 'Api is running'});
});

module.exports = router;


(function() {
  var io = window.io.connect('http://localhost:1337');
  window.io = io;

  io.on('sessions', function(data) {
    console.log(data);
  });

  io.on('tick', function(data) {
    console.log(data);
  });

  io.on('crash', function(data) {
    console.log(data);
  });

  io.on('ready', function(data) {
    console.log(data);
  });

  document.getElementById('host').addEventListener('click', function() {
    console.log('hosting');
    io.emit('host');
  });
  document.getElementById('accept').addEventListener('click', function() {
    console.log('accepting');
    io.emit('accept', document.getElementById('hash').value);
  });
  document.getElementById('ready').addEventListener('click', function() {
    console.log('ready');
    io.emit('ready', document.getElementById('hash').value);
  });
  document.getElementById('tap').addEventListener('click', function() {
    console.log('tapping');
    io.emit('tap', document.getElementById('hash').value);
  });
})();


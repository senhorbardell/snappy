var p2 = require('p2');
var world = new p2.World({
    gravity: [0, -9.82]
});

var circleBody = new p2.Body({
    mass: 5,
    position: [0, 10]
});

var circleShape = new p2.Circle({radius: 2});
circleBody.addShape(circleShape);

world.addBody(circleBody);

var groundBody = new p2.Body({
    mass: 0 //make static
});

var groundShape = new p2.Plane();

groundBody.addShape(groundShape);

world.addBody(groundBody);

var timeStep = 1 / 60;

setInterval(function() {
    world.step(timeStep);
    console.log("Circle y position: " + circleBody.position[1]);
}, 1000 * timeStep);

setInterval(function() {
    circleBody.position[1] = 9
}, 4000);
